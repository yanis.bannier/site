<!DOCTYPE html>
<html lang="en">
<head>
  <title>COMMANDGO, votre communication embarqué</title>

  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="">

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700%7CPT+Serif:400,700,400italic' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/magnific-popup.css" />
  <link rel="stylesheet" href="css/font-icons.css" />
  <link rel="stylesheet" href="revolution/css/settings.css" />
  <link rel="stylesheet" href="css/rev-slider.css" />
  <link rel="stylesheet" href="css/sliders.css">
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/responsive.css" />
  <link rel="stylesheet" href="css/spacings.css" />
  <link rel="stylesheet" href="css/animate.css" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="img/LOGO.svg">
  <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">


  
</head>

<body data-spy="scroll" data-offset="60" data-target=".navbar-fixed-top">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <!-- Navigation -->
  <header class="nav-type-1" id="home">

    <nav class="navbar navbar-fixed-top">
      <div class="navigation-overlay">
        <div class="container-fluid relative">
          <div class="row">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <!-- Logo -->
              <div class="logo-container">
                <div class="logo-wrap local-scroll">
                  <a href="#home">
                    <img class="logo" src="img/AA.png" alt="logo">
                  </a>
                </div>
              </div>
            </div> <!-- end navbar-header -->


            <div class="col-md-8 col-xs-12 nav-wrap">
              <div class="collapse navbar-collapse text-center" id="navbar-collapse">
                
                <ul class="nav navbar-nav local-scroll text-center">

                  <li class="active">
                    <a href="#qui">QUI SOMMES NOUS ?</a>
                  </li>
                  <li>
                    <a href="#concept">LE Concept</a>
                  </li>
                  <li>
                    <a href="#INDICATEURS">INDICATEURS </a>
                  </li>
                  <li>
                    <a href="#about-us">NOS VALEURS</a>
                  </li>                         
                  
                  <li>
                    <a href="#contact">Contact</a>
                  </li>
                  
                </ul>
              </div>
            </div> <!-- end col -->

            <div class="menu-socials hidden-sm hidden-xs">
              <ul>
                <li>
                  
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/comm-go"><i class="fa fa-linkedin"></i></a>
                </li>
                <li>
                  
                </li>
              </ul>
            </div>
            
          </div> <!-- end row -->
        </div> <!-- end container -->
      </div> <!-- end navigation -->
    </nav> <!-- end navbar -->

  </header> <!-- end navigation -->


  <div class="main-wrapper-onepage main oh">


    <!-- Video Background -->
    <section class="hero-wrap video-container text-center">

      <div class="container container-full-height">
        <div class="video-wrap"
          data-vide-bg="mp4: video/commandgo, "
          data-vide-options="posterType: jpg, loop: true, playsinline: true, muted: true, position: 50%">
          <div class="video-overlay"></div>
          <div class="video-img"></div>
        </div>

        <div class="hero-holder">
          <div class="hero-message text-rotator">
            <h1><span class="rotate">DYNAMISEZ VOTRE ACTIVITÉ, AUGMENTEZ VOTRE VISIBILITÉ, IMPACTEZ VOTRE AUDIENCE, CIBLEZ VOS PROSPECTS </span></h1>
            
           
            
          </div>
        </div>
      </div>

    </section> <!-- end video bg -->


    <!-- Intro -->
    


    <!-- Results -->
    

    <section class="page-section  section-wrap  nomargin bg-light " style="padding: 40px;" id="qui">
    
      <div class="container">
          
        <div class="row">
        
          <div class="col-lg-6 col-md-6  fadeInUp wow">
            <div class="promo-device text-center">
              <img src="img/QQQQQ.png" alt="">
            </div>
          </div> <!-- end col -->

          <div class="col-md-6 col-lg-6 " >
          <h3 class="mt-70 pascap " style="letter-spacing: 0em; text-align: justify; " >
          C<span style="color: #EC6611;">O</span>MM&G<span style="color: #EC6611;">O</span> propose la diffusion de <span style="color: #EC6611;">spots publicitaires</span> sur des tablettes installées dans les <span style="color: #EC6611;">Vtc</span> et <span style="color: #EC6611;">Taxis</span> de votre ville.
      
          </h3>
          <br>
          <p style=" font-size: 25px; ">Profitez de nos offres pour toucher vos prospects au cours de leurs déplacements quotidien.  </p>

          <br>
          <br>
            <div class="text-center">


            
              <a href="/PUB.pdf" class="btn btn-md btn-dark2 "  >VISIONNER NOTRE BROCHURE</a>
                
              <!-- <embed src="PUB.pdf" width=800 height=500 type='application/pdf'/> -->
              

              
                
             

             

            </div>
            
          </div> <!-- end col -->   

        </div>
      </div>
    </section> <!-- end features -->

    <!-- Results -->
    <section class="result-boxes section-wrap style-2"  style="background-image: url(img/PHOTO_TOULOUSE_FLOU.png); " id="INDICATEURS">
      <div class="container">
        <div class="row">

          <div class="col-sm-4">
            <div class="statistic result-box">
              <div class="result-wrap clearfix">
                <span class="timer"     data-from="0" data-to="20000"></span>
                <span class="counter-text" >VUES PAR MOIS</span>
              </div>              
            </div>
          </div> <!-- end first result box -->

          <div class="col-sm-4">
            <div class="statistic result-box">
              <div class="result-wrap clearfix">
                <span class="timer" data-from="0" data-to="3"></span>
                <span class="counter-text">EXPOSITIONS minimum par prospect</span>
              </div>            
            </div>
          </div> <!-- end second result box -->

          <div class="col-sm-4">
            <div class="statistic result-box">
              <div class="result-wrap clearfix">
                <span class="timer" data-from="0" data-to="3600"></span>
                <span class="counter-text"> vues minimum par annonce</span> 
              </div>            
            </div>
          </div> <!-- end third result box -->

          

        </div>
      </div>
    </section> <!-- end results-->

    <!-- Our Services -->
    


    <!-- Promo Section -->
    <section class="section-wrap-mp promo-section " style="padding: 40px;" id="concept">

      <div id="owl-promo" class="owl-carousel owl-theme">

        <div class="item">
          <div class="container">
            <div class="row">

              <div class="col-md-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s">
                <img src="img/GG.jpg" alt="">        
              </div>
              
              <div class="col-md-6 ">
                
                
                <h3 class=" pascap " style="letter-spacing: 0em; text-align: justify; "   >Après plus d’<span style="color: #EC6611;">un an d’analyse</span> des passagers transportés dans les véhicules de nos chauffeurs partenaires, nous avons mis en place une <span style="color: #EC6611;">solution de diffusion</span> publicitaire divisée en <span style="color: #EC6611;">sept créneaux horaires prédéfinis</span>.<br>
                <br>
              
                
              </h3>
                <p style=" font-size: 25px; ">Ces différents créneaux permettent un ciblage optimal suivant le type de clientèle que vous visez.
                <br> 
                <br>Il vous suffit donc de choisir le créneau qui correspond au mieux à votre ciblage.</p> 
                
                
                


               

                
              </div>
              
            </div>
          </div>
        </div>

       

      </div> <!-- end slider -->
    </section> <!-- end promo section -->
    

    <!-- Portfolio -->
   


   


    <!-- Process -->
    <section class="section-wrap bg-light pb-mdm-50 pb-130" style="padding: 40px;" id="about-us">
      <div class="container">

        <div class="row heading">
          <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center bottom-line">NOTRE RAISON D'ÊTRE</h2>
            <p class="subheading text-center">Apporter une réelle plus value à l'ensemble de nos partenaires autour de trois valeurs : 
            <br>  
            <br>PROXIMITÉ - VISIBILITÉ - CONVIVIALITÉ </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4 service-item">
            <div class="service-item-box icon-effect-1 icon-effect-1a text-center">
              <a href="#">
                <i class="fa fa-building hi-icon"></i>
              </a>
              <h3>ANNONCEURS</h3>
              <p> 

Nous permettons une diffusion ciblée de vos spots publicitaires sur nos tablettes numériques installées dans les véhicules de nos chauffeurs partenaires.
Choisissez le créneau de diffusion le plus adapté à vos cibles et  
bénéficiez d'un suivi d'audience. Vous serez informés mensuellement de l'impact marketing de votre campagne publicitaire.   </p>     
            </div>            
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <div class="service-item-box icon-effect-1 icon-effect-1a text-center">
              <a href="#">
                <i class="fa fa-taxi hi-icon" ></i>
              </a>
              <h3>CHAUFFEURS PARTENAIRES</h3>
              <p>Nous améliorons la satisfaction client de nos chauffeurs partenaires en apportant un service innovant a leurs passagers. 
              <br>
              <br>Nos partenaires sont rémunérés mensuellement en échange de leurs services. </p>     
              <br>
              <br>
              
            </div>            
          </div> <!-- end service item -->

          <div class="col-md-4 service-item">
            <div class="service-item-box icon-effect-1 icon-effect-1a text-center">
              <a href="#">
                <i class="fa fa-users hi-icon"></i>
              </a>
              <h3>PASSAGERS</h3>
              <p>Nous offrons de multiples avantages aux passagers durant leurs trajets. 
          Ils peuvent recharger leurs téléphones et bénéficier d'un accès internet.
          <br>
              <br> Du contenu touristique et culturel est aussi diffusé afin de les tenir informés des différentes tendances.</p>     
              <br>
              
            </div>            
          </div> <!-- end service item -->

          

          

          
          
        </div>
      </div>
    </section> <!-- end services -->

    <!-- Our Team -->
    <!-- end our team -->


    <!-- About us / Progress Bars -->
    <!-- end progress bars -->


    


    <!-- Testimonials -->
    


    

    <!-- Contact -->
    <section class="section-wrap contact" style="padding: 40px;" id="contact">
      <div class="container">

        

        <div class="row">

          <div class="col-md-4">
            <h5>CONTACTEZ NOUS</h5>
            <p>Lundi au vendredi: 08h-20h</p>

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Pointer"></i>
              </div>
              <h6>Adresse</h6>
              <p>3 Allée du Portel<br>
              31770 COLOMIERS </p>
            </div> <!-- end address -->

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Phone"></i>
              </div>
              <h6>TELEPHONE</h6>
              <span>05 61 32 67 68</span>
            </div> <!-- end phone number -->

            <div class="contact-item">
              <div class="contact-icon">
                <i class="icon icon-Mail"></i>
              </div>
              <h6>E-mail</h6>
              <a href="mailto:contact@commandgo.fr">contact@commandgo.fr</a>
            </div> <!-- end email -->

          </div>

          <div class="col-md-8">
            <form id="contact-form" action="#">

              <div class="row contact-row">
                <div class="col-md-6 contact-name">
                  <input name="name" id="name" type="text" placeholder="Nom*">
                </div>
                <div class="col-md-6 contact-email">
                  <input name="mail" id="mail" type="email" placeholder="E-mail*">
                </div>
              </div>

              <input name="subject" id="subject" type="text" placeholder="Objet*"> 
              <textarea name="comment" id="comment" placeholder="Message"></textarea>
              <input type="submit" class="btn btn-lg btn-color btn-submit " value="ENVOYER" id="submit-message">
              <div id="msg" class="message"></div>
            </form>
          </div> <!-- end col -->

        </div>
      </div>
    </section> <!-- end contact -->


    <!-- Google Map -->
   

    <!-- Footer -->
    <footer class="footer footer-type-2 bg-dark" >
      <div class="container">
        <div class="footer-widgets">
          <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="widget footer-links">
              
                
                <img src="img/BAS_SITE.png"  style="margin : 10px 50px 20px ">
                
              </div>
            </div> <!-- end latest posts -->

            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="widget footer-get-in-touch">
                <h5>Coordonées</h5>
                <p class="footer-address">3 Allée du portel<br> 31770 COLOMIERS </p>
                <p>05 61 32 67 68</p>
                <p>Email: <a href="mailto:Enigmasupport@gmail.com">contact@commandgo.fr</a></p>
                
              </div>
            </div> <!-- end stay in touch -->

            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="widget footer-links">
                <h5>Liens utiles</h5>
                <ul>
                  <li><a href="#qui">Qui sommes nous ?</a></li>
                  <li><a href="#concept">Le concept</a></a></li>
                  <li><a href="#about-us">Nos valeurs</a></li>
                  <li><a href="#kpi">Mention légal</a></li>
                  
                </ul>
              </div>
            </div> <!-- end useful links -->

            

          </div> <!-- end row -->
        </div> <!-- end footer widgets -->      
      </div> <!-- end container -->

      <div class="bottom-footer">
        <div class="container">
          <div class="row">

            <div class="col-sm-6 col-xs-12 copyright">
              <span>
                &copy;<script>document.write( new Date().getFullYear() );</script> Enigma  |  Development by <a href="https://deothemes.com">DeoThemes</a>
              </span>
            </div>
              
            <div class="col-sm-4 col-sm-offset-2 col-xs-12 footer-socials">
              <div class="social-icons clearfix right">
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-facebook"></i></a>                  
                <a href="#"><i class="fa fa-google-plus"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa-vimeo"></i></a>
              </div>
            </div>

          </div>
        </div>
      </div> <!-- end bottom footer -->
    </footer> <!-- end footer -->


    <div id="back-to-top">
      <a href="#top"><i class="fa fa-angle-up"></i></a>
    </div>

  </div> <!-- end main-wrapper -->
  
  <!-- jQuery Scripts -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- 
    1. Generate your key here - https://developers.google.com/maps/documentation/javascript/get-api-key
    2. Paste your key in the script below.
  -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoQ3_zzRfW-hYspkwr5kvwCwLPGZsN4nw"></script>
  <script type="text/javascript" src="js/gmap3.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>

  <!-- Google Map -->
  <script type="text/javascript">
    $(document).ready( function(){

      var gmapDiv = $("#google-map");
      var gmapMarker = gmapDiv.attr("data-address");

      gmapDiv.gmap3({
        zoom: 16,
        address: gmapMarker,
        oomControl: true,
        navigationControl: true,
        scrollwheel: false,
        styles: [
          {
          "featureType":"all",
          "elementType":"all",
            "stylers":[
              { "saturation":"-70" }
            ]
        }]
      })
      .marker({
        address: gmapMarker,
        icon: "img/map_pin.png"
      })
      .infowindow({
        content: "V Tytana St, Manila, Philippines"
      })
      .then(function (infowindow) {
        var map = this.get(0);
        var marker = this.get(1);
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      });
    });
  </script>

</body>
</html>